<?php

declare(strict_types=1);

namespace RR\EventBusBundle\DependencyInjection\Compiler;

use RR\EventBusBundle\DependencyInjection\Route\MessageProcessorLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class MessageProcessorLocatorPass
 * @package RR\EventBusBundle\DependencyInjection\Compiler
 */
class MessageProcessorLocatorPass implements CompilerPassInterface
{
    public const MESSAGE_PROCESSOR_TAG = 'rr.event_bus.message_processor';

    /**
     * @inheritDoc
     */
    public function process(ContainerBuilder $container): void
    {
        if (!$container->has(MessageProcessorLocator::class)) {
            return;
        }

        $definition = $container->findDefinition(MessageProcessorLocator::class);

        $taggedServices = $container->findTaggedServiceIds(self::MESSAGE_PROCESSOR_TAG);

        foreach ($taggedServices as $id => $tags) {
            $container->getDefinition($id)->setLazy(true);
            $definition->addMethodCall('addProcessor', [$id, new Reference($id)]);
        }
    }
}
