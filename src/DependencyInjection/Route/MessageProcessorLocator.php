<?php

declare(strict_types=1);

namespace RR\EventBusBundle\DependencyInjection\Route;

use RR\EventBus\MessageProcessorInterface;

/**
 * Class MessageHandlerLocator
 * @package RR\EventBusBundle\DependencyInjection\Route
 */
class MessageProcessorLocator
{
    private $processors = [];

    /**
     * @param string $class
     * @return MessageProcessorInterface
     */
    public function __invoke(string $class): MessageProcessorInterface
    {
        return $this->processors[$class] ?? new $class();
    }

    /**
     * @param string $id
     * @param MessageProcessorInterface $processor
     */
    public function addProcessor(string $id, MessageProcessorInterface $processor): void
    {
        $this->processors[$id] = $processor;
    }
}
