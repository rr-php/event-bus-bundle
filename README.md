## Install

Add to composer.json

```
"repositories": [
    {
        "type": "vcs",
        "url": "https://gitlab.com/rr-php/event-bus-bundle.git"
    },
    {
        "type": "vcs",
        "url": "https://gitlab.com/rr-php/event_bus.git"
    }
],
```

`composer require rr/event-bus-bundle`

**TODO:** Make flex recipe (https://github.com/symfony/recipes/blob/master/README.rst) 

```php
// <project>>/config/bundles.php
return [
    //...
    RR\EventBusBundle\RrEventBusBundle::class => ['all' => true],
];
```

```yaml
# <project>>/config/packages/rr_event_bus.yaml

rr_event_bus:
    default_connection: my_kafka
    connections:
        my_kafka:
            # Connection config
            config:
                driver: kafka
                # @see https://github.com/edenhill/librdkafka/blob/master/CONFIGURATION.md
                rdkafka_conf:
                    metadata.broker.list: '%env(KAFKA_BROKERS)%'
                    enable.auto.commit: 'false'
                    group.id: api
                    auto.offset.reset: smallest
                    offset.store.method: broker
                    queued.max.messages.kbytes: 100000
                    topic.metadata.refresh.sparse: 'true'
                    topic.metadata.refresh.interval.ms: 600000
                    auto.commit.enable: 'false'
                timeout: 120000
                prefix: '%env(KAFKA_PREFIX)%'
            # Service config
            service:
                # Optional: If set to false, the service and its alias can only be
                # used via dependency injection, and not be retrieved from the
                # container directly. By default `false`.
                public: true
```

## Example consumer

```bash
php bin/console rr:event_bus:listen my_kafka
```
If connection name not provided, then default connection will be used.

```php
class MessageHandler implements \RR\EventBus\MessageProcessorInterface
{
    public function __construct(/* DI available*/){
        
    }

    public function process(\RR\EventBus\Message $message): bool
    {
        // ...
    }
}

class KafkaRouteProvider implements \RR\EventBusBundle\DependencyInjection\Route\RouteProviderInterface
{
    public function getConnectionName(): string
    {
        return 'my_kafka';
    }

    
    public function register(\RR\EventBus\BusRouter $router): void
    {
        $router->add('topic_name', MessageHandler::class);
    }
}
```

## Example producer

```
// injection
__construct(\RR\EventBus\ProducerInterface $producer) {
    // producer with default connection or producer with specific connection if argument set in services.yaml
}

// service container
__construct(\Symfony\Component\DependencyInjection\ContainerInterface $container) {
    $producer = $container->get('rr_event_bus.<connection name>.producer');
    // Only if service is public
}

// factory
__construct(\RR\EventBusBundle\Service\EventBusFactory $factory) {
    $config = [/* see rr_event_bus.connections.<connection>.config in <project>>/config/packages/rr_event_bus.yaml */];
    $producer = $factory->createProducer($config);
}   
    
$producer->produce('topic', 'message', 'some_key_to_enable_correct_message_order')

```



