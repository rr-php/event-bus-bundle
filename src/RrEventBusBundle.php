<?php

declare(strict_types=1);

namespace RR\EventBusBundle;

use RR\EventBus\MessageProcessorInterface;
use RR\EventBusBundle\DependencyInjection\Compiler\EventBusRouterPass;
use RR\EventBusBundle\DependencyInjection\Compiler\MessageProcessorLocatorPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use RR\EventBusBundle\DependencyInjection\RrEventBusExtension;
use RR\EventBusBundle\DependencyInjection\Route\RouteProviderInterface;

/**
 * Class RrEventBusBundle
 * @package RR\EventBusBundle
 */
class RrEventBusBundle extends Bundle
{
    /**
     * @inheritDoc
     */
    public function build(ContainerBuilder $container): void
    {
        parent::build($container);

        $container
            ->addCompilerPass(new EventBusRouterPass())
            ->addCompilerPass(new MessageProcessorLocatorPass());

        $container
            ->registerForAutoconfiguration(RouteProviderInterface::class)
            ->addTag(RouteProviderInterface::TAG);

        $container
            ->registerForAutoconfiguration(MessageProcessorInterface::class)
            ->addTag(MessageProcessorLocatorPass::MESSAGE_PROCESSOR_TAG);
    }

    /**
     * @inheritDoc
     */
    public function getContainerExtension(): ExtensionInterface
    {
        return new RrEventBusExtension();
    }
}
