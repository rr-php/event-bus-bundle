<?php

declare(strict_types=1);

namespace RR\EventBusBundle\DependencyInjection\Compiler;

use RR\EventBus\BusRouter;
use RR\EventBusBundle\Service\EventBusRouter;
use RR\EventBusBundle\DependencyInjection\Route\RouteProviderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class EventBusRouterPass
 * @package RR\EventBusBundle\DependencyInjection\Compiler
 */
class EventBusRouterPass implements CompilerPassInterface
{
    /**
     * @inheritDoc
     */
    public function process(ContainerBuilder $container): void
    {
        if (!$container->has(EventBusRouter::class)) {
            return;
        }

        $definition = $container->findDefinition(EventBusRouter::class);

        $taggedServices = $container->findTaggedServiceIds(RouteProviderInterface::TAG);

        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('registerRouteProvider', [new Reference($id), new Reference(BusRouter::class)]);
        }
    }
}
