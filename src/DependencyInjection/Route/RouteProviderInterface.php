<?php

declare(strict_types=1);

namespace RR\EventBusBundle\DependencyInjection\Route;

use RR\EventBus\BusRouter;

/**
 * Interface EventBusRouteProviderInterface
 * @package RR\EventBusBundle\DependencyInjection\Route
 */
interface RouteProviderInterface
{
    public const TAG = 'rr.event_bus.route_provider';

    /**
     * @return string
     */
    public function getConnectionName(): string;

    /**
     * @param BusRouter $router
     */
    public function register(BusRouter $router): void;
}
