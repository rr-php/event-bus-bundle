<?php

declare(strict_types=1);

namespace RR\EventBusBundle\Service;

use RR\EventBus\ConsumerInterface;
use RR\EventBus\ProducerInterface;
use RR\EventBus\Transports;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class EventBusFactory
 * @package RR\EventBusBundle\Service
 */
final class EventBusFactory
{
    protected const CONSUMER = 'consumer';
    protected const PRODUCER = 'producer';

    /** @var array */
    protected $drivers;

    public function __construct()
    {
        $this->drivers = Transports::map();
    }

    /**
     * @return array
     */
    public function getDrivers(): array
    {
        return $this->drivers;
    }

    /**
     * @param string $name
     * @param ConsumerInterface $consumer
     * @param ProducerInterface $producer
     * @return EventBusFactory
     */
    public function addDriver(string $name, ConsumerInterface $consumer, ProducerInterface $producer): EventBusFactory
    {
        $this->drivers[$name] = [
            self::CONSUMER => \get_class($consumer),
            self::PRODUCER => \get_class($producer),
        ];

        return $this;
    }

    /**
     * @param array $config
     * @return ConsumerInterface
     */
    public function createConsumer(array $config): ConsumerInterface
    {
        $class = $this->getProcessClassName(self::CONSUMER, $config);

        return new $class($config);
    }

    /**
     * @param array $config
     * @return ProducerInterface
     */
    public function createProducer(array $config): ProducerInterface
    {
        $class = $this->getProcessClassName(self::PRODUCER, $config);

        return new $class($config);
    }

    /**
     * @param string $process
     * @param array $config
     * @return string
     */
    private function getProcessClassName(string $process, array $config): string
    {
        $this->assertThatConfigIsValid($config);

        $driver = $config['driver'];
        $this->assertThatDriverSupported($driver);

        return $this->drivers[$driver][$process];
    }

    /**
     * @param array $config
     */
    private function assertThatConfigIsValid(array $config): void
    {
        $resolver = new OptionsResolver();
        $resolver
            ->setDefined(array_keys($config))
            ->setRequired('driver')
            ->setAllowedTypes('driver', 'string')
            ->setAllowedValues('driver', array_keys($this->drivers));

        $resolver->resolve($config);
    }

    /**
     * @param string $driver
     */
    private function assertThatDriverSupported(string $driver): void
    {
        if (!\array_key_exists($driver, $this->drivers)) {
            throw new \InvalidArgumentException("Driver `$driver` not supported.");
        }
    }
}
