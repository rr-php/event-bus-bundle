<?php

declare(strict_types=1);

namespace RR\EventBusBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 * @package RR\EventBusBundle\DependencyInjection
 */
class Configuration implements ConfigurationInterface
{
    /** @var string */
    private $name;

    /**
     * Configuration constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @inheritDoc
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $builder = new TreeBuilder($this->name);

        if (\method_exists($builder, 'getRootNode')) {
            $root = $builder->getRootNode();
        } else {
            $root = $builder->root($this->name);
        }

        $root
            ->fixXmlConfig('connection')
            ->children()
                ->scalarNode('default_connection')
                    ->isRequired()
                    ->cannotBeEmpty()
                ->end()
                ->arrayNode('connections')
                    ->useAttributeAsKey('name')
                    ->arrayPrototype()
                        ->children()
                            ->arrayNode('config')
                                ->ignoreExtraKeys(false)
                                ->children()
                                    ->scalarNode('driver')
                                        ->isRequired()
                                        ->cannotBeEmpty()
                                    ->end()
                                ->end()
                            ->end()
                            ->arrayNode('service')
                                ->children()
                                    ->booleanNode('public')->defaultFalse()->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $builder;
    }
}
