<?php

declare(strict_types=1);

namespace RR\EventBusBundle\DependencyInjection;

use RR\EventBus\BusRouter;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;
use RR\EventBus\ConsumerInterface;
use RR\EventBus\ProducerInterface;
use RR\EventBusBundle\Service\EventBusFactory;
use RR\EventBusBundle\Command\BusDaemonCommand;

/**
 * Class RrEventBusExtension
 * @package RR\EventBusBundle\DependencyInjection
 */
class RrEventBusExtension extends Extension
{
    protected const ALIAS = 'rr_event_bus';

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = $this->getConfiguration($configs, $container);
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new XmlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.xml');

        $connectionsList = $config['connections'] ?? [];
        $this->assertThatAtLeastOneConnectionExists($connectionsList);

        $defaultConnection = $config['default_connection'] ?? '';
        $this->assertThatDefaultConnectionExists($connectionsList, $defaultConnection);

        foreach ($connectionsList as $name => $connectionConfig) {
            $connectionConfig['service']['default'] = $name === $defaultConnection;
            $this->processConnectionConfiguration($name, $connectionConfig, $container);
        }

        $commandDefinition = $container->getDefinition(BusDaemonCommand::class);
        $commandDefinition->addMethodCall('setConfig', [$defaultConnection, $connectionsList]);
    }

    /**
     * @inheritDoc
     */
    public function getAlias(): string
    {
        return self::ALIAS;
    }

    /**
     * @inheritDoc
     */
    public function getConfiguration(array $config, ContainerBuilder $container): Configuration
    {
        return new Configuration($this->getAlias());
    }

    /**
     * @param array $connectionsList
     */
    private function assertThatAtLeastOneConnectionExists(array $connectionsList): void
    {
        if (count($connectionsList) === 0) {
            throw new InvalidConfigurationException('At least one connection must exist.');
        }
    }

    /**
     * @param array $connectionsList
     * @param string $defaultConnection
     */
    private function assertThatDefaultConnectionExists(array $connectionsList, string $defaultConnection): void
    {
        if (!\array_key_exists($defaultConnection, $connectionsList)) {
            throw new InvalidConfigurationException(
                "Invalid default connection. Connection `$defaultConnection` does not exist."
            );
        }
    }

    /**
     * @param string $name
     * @param array $config
     * @param ContainerBuilder $container
     */
    private function processConnectionConfiguration(string $name, array $config, ContainerBuilder $container): void
    {
        $this->registerService($name . '.consumer', $config, ConsumerInterface::class, $container, 'createConsumer');
        $this->registerService($name . '.producer', $config, ProducerInterface::class, $container, 'createProducer');
    }

    /**
     * @param string $name
     * @param array $config
     * @param string $class
     * @param ContainerBuilder $container
     * @param string $method
     * @return string
     */
    private function registerService(
        string $name,
        array $config,
        string $class,
        ContainerBuilder $container,
        string $method
    ): string {
        $serviceId = $this->getAlias() . '.' . $name;
        $isPublic = $config['service']['public'] ?? false;
        $isDefault = $config['service']['default'];

        $factory = $container->getDefinition(EventBusFactory::class);

        $container->register($serviceId, $class)
            ->setFactory([$factory, $method])
            ->addArgument($config['config'])
            ->setPublic($isPublic);

        if ($isDefault) {
            $container->setAlias($class, $serviceId);
            $container->getAlias($class)->setPublic($isPublic);
        }

        return $serviceId;
    }
}
