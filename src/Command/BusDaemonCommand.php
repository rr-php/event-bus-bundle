<?php

declare(strict_types=1);

namespace RR\EventBusBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use RR\EventBusBundle\Service\EventBusFactory;
use RR\EventBusBundle\Service\EventBusRouter;
use LogicException;

/**
 * Class BusDaemonCommand
 * @package RR\EventBusBundle\Command
 */
class BusDaemonCommand extends Command
{
    /** @var string */
    protected static $defaultName = 'rr:event_bus:listen';

    /** @var EventBusFactory */
    private $busFactory;

    /** @var EventBusRouter */
    private $busRouter;

    /** @var string */
    protected $defaultConnection = '';

    /** @var array */
    protected $connectionsList = [];

    /**
     * BusDaemonCommand constructor.
     * @param EventBusFactory $busFactory
     * @param EventBusRouter $busRouter
     */
    public function __construct(EventBusFactory $busFactory, EventBusRouter $busRouter)
    {
        parent::__construct();

        $this->busFactory = $busFactory;
        $this->busRouter = $busRouter;
    }

    /**
     * @param string $defaultConnection
     * @param array $connectionsList
     */
    public function setConfig(string $defaultConnection, array $connectionsList): void
    {
        $this->defaultConnection = $defaultConnection;
        $this->connectionsList = $connectionsList;
    }

    /**
     * @inheritDoc
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Message broker daemon')
            ->setHelp('This command allows you to consume messages from some message broker.')
            ->addArgument('connection', InputArgument::OPTIONAL, 'Connection name')
        ;
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $connectionName = (string)$input->getArgument('connection');

        if (!$connectionName) {
            $connectionName = $this->defaultConnection;
            $output->writeln("Using the default connection: `$connectionName`");
        }

        $config = $this->connectionsList[$connectionName]['config'] ?? null;

        if ($config === null) {
            throw new InvalidArgumentException("Config for connection `$connectionName` does not provided.");
        }

        if (!$this->busRouter->has($connectionName)) {
            throw new LogicException("There are no routes for `$connectionName` event bus connection.");
        }

        $consumer = $this->busFactory->createConsumer($config);
        $consumer->consume($this->busRouter->get($connectionName));

        return 0;
    }
}
