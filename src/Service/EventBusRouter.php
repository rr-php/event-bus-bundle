<?php

declare(strict_types=1);

namespace RR\EventBusBundle\Service;

use RR\EventBusBundle\DependencyInjection\Route\RouteProviderInterface;
use RR\EventBus\BusRouter;

/**
 * Class EventBusRouter
 * @package RR\EventBusBundle\Service
 */
final class EventBusRouter
{
    /** @var array */
    protected $connections = [];

    /**
     * @param RouteProviderInterface $routeProvider
     * @param BusRouter $router
     */
    public function registerRouteProvider(RouteProviderInterface $routeProvider, BusRouter $router): void
    {
        $routeProvider->register($router);
        $this->connections[$routeProvider->getConnectionName()] = $router;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function has(string $name): bool
    {
        return \array_key_exists($name, $this->connections);
    }

    /**
     * @param string $name
     * @return BusRouter|null
     */
    public function get(string $name): ?BusRouter
    {
        return $this->connections[$name] ?? null;
    }
}
